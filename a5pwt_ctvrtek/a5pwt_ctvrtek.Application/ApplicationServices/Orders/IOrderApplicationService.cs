﻿using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Application.ApplicationServices.Orders
{
    public interface IOrderApplicationService
    {
        void CreateOrder(int userId, string userTrackingCode);
    }
}
