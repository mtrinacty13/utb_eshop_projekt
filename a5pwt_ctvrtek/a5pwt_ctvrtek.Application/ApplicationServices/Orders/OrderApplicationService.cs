﻿using a5pwt_ctvrtek.Domain.Services.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Application.ApplicationServices.Orders
{
    public class OrderApplicationService : IOrderApplicationService
    {
        private readonly IOrderService _orderService;
        public OrderApplicationService(IOrderService orderService)
        {
            _orderService = orderService;
        }
        public void CreateOrder(int userId, string userTrackingCode)
        {
            _orderService.CreateOrder(userId, userTrackingCode);
        }
    }
}
