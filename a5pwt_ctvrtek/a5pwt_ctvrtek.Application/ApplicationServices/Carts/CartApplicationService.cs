﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a5pwt_ctvrtek.Application.Mappers.Carts;
using a5pwt_ctvrtek.Application.ViewModels.Carts;
using a5pwt_ctvrtek.Domain.Entities.Carts;
using a5pwt_ctvrtek.Domain.Services.Carts;
using a5pwt_ctvrtek.Domain.Constants;
using System.Security.Claims;
using a5pwt_ctvrtek.Application.ApplicationServices.Security;
using Microsoft.AspNetCore.Identity;
using a5pwt_ctvrtek.Infrastructure.Identity.Users;
using a5pwt_ctvrtek.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using a5pwt_ctvrtek.Application.Helpers;

namespace a5pwt_ctvrtek.Application.ApplicationServices.Carts
{
    public class CartApplicationService : ICartApplicationService
    {
        private readonly ICartService _cartService;
        private readonly ICartMapper _cartMapper;

		private readonly SignInManager<User> _signInManager;

		public CartApplicationService(ICartService cartService, ICartMapper cartMapper, SignInManager<User> signInManager)
        {
            _cartService = cartService;
            _cartMapper = cartMapper;
			_signInManager = signInManager;
			
		}
        public CartItem AddToCart(int productID, int amount, string userTrackingCode)//pridat slevovy koeficient
        {
            return _cartService.AddToCart(productID, amount, userTrackingCode);
        }

        public IndexViewModel GetIndexViewModel(string userTrackingCode)
        {
			//Helper h = new Helper();
            var items = _cartService.GetCartItems(userTrackingCode);
            return new IndexViewModel
            {
                CartItems= _cartMapper.GetViewModelsFromEntities(items),
				Total = Helper.CountTotalSum(items,_signInManager)//items.Sum(x => x.Product.Price * x.Amount)
            };
        }
		
        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            _cartService.RemoveFromCart(productID, userTrackingCode);
        }
		
	}
}
