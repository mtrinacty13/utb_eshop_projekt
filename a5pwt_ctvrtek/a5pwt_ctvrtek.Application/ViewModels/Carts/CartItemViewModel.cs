﻿namespace a5pwt_ctvrtek.Application.ViewModels.Carts
{
    public class CartItemViewModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ImageUrl { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
		public decimal Discount { get; set; }
		public decimal TotalPrice { get; set; }
		//slevovy koeficient decimal
    }
}