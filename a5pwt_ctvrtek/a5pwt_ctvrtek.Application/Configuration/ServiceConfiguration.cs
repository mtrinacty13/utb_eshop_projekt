﻿using a5pwt_ctvrtek.Infrastructure.Configuration;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace a5pwt_ctvrtek.Application.Configuration
{
    public class ServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment enviroment)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(ServiceConfiguration)));
            services.AddSingleton(enviroment);
            InfrastractureServiceConfiguration.Load(services,enviroment);
        }
    }
}
