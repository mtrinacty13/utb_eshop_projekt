﻿using a5pwt_ctvrtek.Application.ViewModels.Carts;
using a5pwt_ctvrtek.Domain.Entities.Carts;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using a5pwt_ctvrtek.Domain.Constants;
namespace a5pwt_ctvrtek.Application.Configuration.Profiles
{
    public class CartProfile :Profile
    {
        public CartProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<CartItem, CartItemViewModel>()
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Product.ImageURL))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(dest => dest.ProductID, opt => opt.MapFrom(src => src.Product.ID))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Product.Price))
				.ForMember(dest => dest.Discount, opt => opt.MapFrom(src => src.Discount))
				.ForMember(dest => dest.TotalPrice, opt => opt.MapFrom(src=> src.TotalPrice));

			//pridat slevovy koeficient
		}
		private decimal Discount(int amount)
		{
			if (amount > 0 && amount < Pricing.FirstClassAmount)
			{
				return Pricing.Base;
			}
			else if (amount >= Pricing.FirstClassAmount && amount < Pricing.SecondClassAmount)
			{
				return Pricing.FirstClass;
			}
			else if (amount >= Pricing.SecondClassAmount && amount < Pricing.ThirdClassAmount)
			{
				return Pricing.SecondClass;
			}
			else
			{
				return Pricing.ThirdClass;
			}
		}
	}
}
