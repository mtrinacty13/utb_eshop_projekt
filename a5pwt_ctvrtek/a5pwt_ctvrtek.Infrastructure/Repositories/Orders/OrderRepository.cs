﻿using a5pwt_ctvrtek.Domain.Entities.Orders;
using a5pwt_ctvrtek.Domain.Repositories.Carts;
using a5pwt_ctvrtek.Domain.Repositories.Orders;
using a5pwt_ctvrtek.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace a5pwt_ctvrtek.Infrastructure.Repositories.Orders
{
    class OrderRepository : IOrderRepository
    {
        private readonly ICartRepository _cartRepository;
        private readonly DataContext _dataContext;
        private readonly DbSet<Order> _dbSet;

        public OrderRepository(ICartRepository cartRepository, DataContext dataContext)
        {
            _cartRepository = cartRepository;
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Order>();
        }

        public void CreateOrder(int userId, string userTrackingCode)
        {
            var cart = _cartRepository.GetCurrentCart(userTrackingCode);
            var order = new Order
            {
                UserID = userId,
                OrderItems = new List<OrderItem>(),
                OrderNumber = userTrackingCode.Take(8).ToString()
            };
            foreach (var item in cart.CartItems)
            {
                order.OrderItems.Add(new OrderItem
                {
                    ProductID = item.ProductID
                });
            }
            _dbSet.Add(order);
   
            _dataContext.SaveChanges();
        }
    }
}
