﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a5pwt_ctvrtek.Domain.Entities.Carts;
using a5pwt_ctvrtek.Domain.Repositories.Carts;
using a5pwt_ctvrtek.Infrastructure.Data;
using a5pwt_ctvrtek.Domain.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using a5pwt_ctvrtek.Infrastructure.Identity.Users;
using a5pwt_ctvrtek.Infrastructure.Helpers;
using a5pwt_ctvrtek.Infrastructure.Repositories.Products;

namespace a5pwt_ctvrtek.Infrastructure.Repositories.Carts
{
    public class CartRepository : ICartRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Cart> _dbSet;
		private readonly SignInManager<User> _signInManager;
		public CartRepository(DataContext dataContext,SignInManager<User> signInManager)
        {
            _dataContext = dataContext;
            _dbSet = dataContext.Set<Cart>();
			_signInManager = signInManager;
        }
		//TODO: FinalPrice vraci spatne hodnoty pro stredni mnozstvi
        public CartItem AddToCart(int productID, int amount, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var cartItem = cart.CartItems.FirstOrDefault(t => t.ProductID == productID);

			ProductRepository productRepository = new ProductRepository(_dataContext);

			var productList = productRepository.GetAll();
			decimal basePrice = productList.FirstOrDefault(t => t.ID == productID).Price;
			decimal productPrice = Helper.RolePricing(basePrice, _signInManager);
			//decimal productPrice = Helper.PricePerOne(amount, Helper.RolePricing(cartItem.Product.Price, _signInManager));
			if (cartItem == null)
            {
				cartItem = new CartItem()
				{
					ProductID = productID,
					Amount = amount,
					CartID = cart.ID,
					Discount = Helper.Discount(amount) * Helper.RolePricingDiscount(_signInManager),
					TotalPrice =Helper.FinalPrice(amount,productPrice)
                };

				

				cart.CartItems.Add(cartItem);
				
				//cartItem.TotalPrice = Helper.FinalPrice(amount, productPrice);

			}
            else
            {
                cartItem.Amount += amount;// v discount bude chyba asi nebo price per , cenove hladiny ve view su debilne
				cartItem.Discount = Helper.Discount(cartItem.Amount)*Helper.RolePricingDiscount(_signInManager);

				productPrice = Helper.RolePricing(basePrice, _signInManager);
				cartItem.TotalPrice = Helper.FinalPrice(cartItem.Amount, productPrice);

			}
            _dataContext.SaveChanges();
            return cartItem;
        }

        public IList<CartItem> GetCartItems(string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            return cart.CartItems;
        }

        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var cartItem = cart.CartItems.FirstOrDefault(t=>t.ProductID==productID);

            if (cartItem == null)
            {
                return;
            }
            cart.CartItems.Remove(cartItem);
            _dataContext.Entry(cartItem).State = EntityState.Deleted;
            _dataContext.SaveChanges();
        }
        public Cart GetCurrentCart(string userTrackingCode)
        {
            var cart = _dbSet
                .Include(x => x.CartItems)
                .ThenInclude(p => p.Product)
                .FirstOrDefault(t => t.UserTrackingCode == userTrackingCode);
            if (cart==null)
            {
                cart = new Cart()
                {
                    UserTrackingCode = userTrackingCode,
                    CartItems = new List<CartItem>()
                };
                _dbSet.Add(cart);
                _dataContext.SaveChanges();
            }
            return cart;
        }
		
    }
}
