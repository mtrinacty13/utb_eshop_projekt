﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using a5pwt_ctvrtek.Domain.Entities.Adresses;

namespace a5pwt_ctvrtek.Infrastructure.Data.Configurations.Addresses
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("Addresses", "Web");
            builder.HasKey(e => e.ID);
        }
    }
}
