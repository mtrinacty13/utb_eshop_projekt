﻿using a5pwt_ctvrtek.Domain.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Infrastructure.Configuration.Orders
{
    class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders", "Web");

            builder.HasKey(e => e.ID);

            builder.HasMany(e => e.OrderItems)
                .WithOne(e => e.Order)
                .IsRequired()
                .HasForeignKey(e => e.OrderID)
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(e => e.BillingAdress)
                .WithMany()
                .IsRequired(false)
                .HasForeignKey(e => e.BillingAdressID)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.ShippingAdress)
                .WithMany()
                .IsRequired(false)
                .HasForeignKey(e => e.ShippingAdressID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
