﻿using a5pwt_ctvrtek.Domain.Constants;
using a5pwt_ctvrtek.Domain.Entities.Carts;
using a5pwt_ctvrtek.Infrastructure.Data;
using a5pwt_ctvrtek.Infrastructure.Identity.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using static a5pwt_ctvrtek.Domain.Constants.Roles;

namespace a5pwt_ctvrtek.Infrastructure.Helpers
{
	public static class Helper
	{

		public static decimal CountTotalSum(IList<CartItem> items, SignInManager<User> signInManager)//predelat na userTrackingCode
		{

			decimal basePrice = items.Sum(x => x.Product.Price);


			decimal tmpPrice = 0;
			decimal finalPrice = 0;

			for (int i = 0; i < items.Count; i++)
			{
				decimal itemPrice = items.ElementAt(i).Product.Price;
				basePrice = RolePricing(itemPrice, signInManager);
				decimal amount = items.ElementAt(i).Amount;
				tmpPrice = FinalPrice(amount, basePrice);
				finalPrice += tmpPrice;
			}
			return finalPrice;
		}

		public static decimal RolePricing(decimal itemPrice, SignInManager<User> signInManager)
		{
			decimal basePrice;
			bool isGolden = false;
			bool isSilver = false;
			try
			{
				string ActualUserId = string.Empty;



				ActualUserId = signInManager.Context.User.Claims.FirstOrDefault().Value;

				List<UserRoles> userRolesList = UserRole();


				foreach (var item in userRolesList)
				{
					if (item.UserID == Convert.ToInt32(ActualUserId) && item.RoleID == Roles.UserGoldenRoleID)
					{
						isGolden = true;
					}
					if (item.UserID == Convert.ToInt32(ActualUserId) && item.RoleID == Roles.UserSilverRoleID)
					{
						isSilver = true;
					}
				}

			}
			catch (Exception e)
			{
			}
			if (isGolden)
			{
				basePrice = itemPrice * Pricing.SecondClass;
			}
			else if (isSilver)
			{
				basePrice = itemPrice * Pricing.FirstClass;
			}
			else
			{
				basePrice = itemPrice;
			}
			return basePrice;
		}
		public static decimal RolePricingDiscount( SignInManager<User> signInManager)
		{
			decimal discount;
			bool isGolden = false;
			bool isSilver = false;
			try
			{
				string ActualUserId = string.Empty;



				ActualUserId = signInManager.Context.User.Claims.FirstOrDefault().Value;

				List<UserRoles> userRolesList = UserRole();


				/*foreach (var item in userRolesList)
				{
					if (item.UserID.ToString() == ActualUserId && item.RoleID == Roles.UserGoldenRoleID)
					{
						isGolden = true;
					}
					if (item.UserID == Convert.ToInt32(ActualUserId) && item.RoleID == Roles.UserSilverRoleID)
					{
						isSilver = true;
					}
				}*/
				for (int i = 0; i < userRolesList.Count(); i++)
				{
					
					if (userRolesList.ElementAt(i).UserID == Convert.ToInt32(ActualUserId) && userRolesList.ElementAt(i).RoleID == Roles.UserGoldenRoleID)
					{
						isGolden = true;
					}
					if (userRolesList.ElementAt(i).UserID == Convert.ToInt32(ActualUserId) && userRolesList.ElementAt(i).RoleID == Roles.UserSilverRoleID)
					{
						isSilver = true;
					}
				}

			}
			catch (Exception e)
			{
			}
			if (isGolden)
			{
				discount =  Pricing.SecondClass;
			}
			else if (isSilver)
			{
				discount = Pricing.FirstClass;
			}
			else
			{
				discount = 1;
			}
			return discount;
		}
		private static List<UserRoles> UserRole()
		{
			List<UserRoles> userRolesList = new List<UserRoles>();
			var builder = new DbContextOptionsBuilder<DataContext>();
			DataContext dbContext = new DataContext(builder.UseSqlServer("Server=databaze.fai.utb.cz;Database=A17635_A5PWT;User ID=A17635;Password=a5pwttrinacty;persist security info=True;multipleactiveresultsets=True").Options);
			var users = dbContext.UserRoles;
			var userRoles = users.ToList();
			foreach (var item in userRoles)
			{
				userRolesList.Add(
					new UserRoles
					{
						UserID = item.UserId,
						RoleID = item.RoleId
					});
			}
			return userRolesList;
		}
		public static decimal FinalPrice(decimal amount, decimal price)
		{

			decimal finalPrice = 0;
			if (amount >= Pricing.BaseAmount && amount < Pricing.FirstClassAmount)
			{
				finalPrice = amount * price;
			}
			else if (amount >= Pricing.FirstClassAmount && amount < Pricing.SecondClassAmount)
			{
				finalPrice = amount * (price * Pricing.FirstClass);
			}
			else if (amount >= Pricing.SecondClassAmount && amount < Pricing.ThirdClassAmount)
			{
				finalPrice = amount * (price * Pricing.SecondClass);
			}

			else if (amount > Pricing.ThirdClassAmount)
			{
				finalPrice = amount * (price * Pricing.ThirdClass);
			}
			return finalPrice;
		}

		
		public static decimal Discount(decimal amount)
		{
			decimal discount = 0;
			if (amount > 0 && amount < Pricing.FirstClassAmount)
			{
				discount = Pricing.Base;
			}
			else if (amount >= Pricing.FirstClassAmount && amount < Pricing.SecondClassAmount)
			{
				discount = Pricing.FirstClass;
			}
			else if (amount >= Pricing.SecondClassAmount && amount < Pricing.ThirdClassAmount)
			{
				discount = Pricing.SecondClass;
			}
			else if (amount >= Pricing.ThirdClassAmount)
			{
				discount = Pricing.ThirdClass;
			}
			return discount;
		}
	}
}
