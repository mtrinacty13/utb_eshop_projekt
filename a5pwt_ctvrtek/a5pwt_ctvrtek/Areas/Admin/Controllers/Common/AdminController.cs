﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace a5pwt_ctvrtek.Areas.Admin.Controllers.Common
{
    [Authorize(Roles = "Manager, Admin"), Area("Admin")]
    public class AdminController : Controller
    {
        
    }
}