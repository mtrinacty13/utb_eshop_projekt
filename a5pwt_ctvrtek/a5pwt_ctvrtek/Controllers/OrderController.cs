﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using a5pwt_ctvrtek.Application.ApplicationServices.Orders;
using a5pwt_ctvrtek.Application.ApplicationServices.Security;
using a5pwt_ctvrtek.Domain.Constants;
using Microsoft.AspNetCore.Mvc;

namespace a5pwt_ctvrtek.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderApplicationService _orderApplicationService;
        private readonly IApplicationSecurityService _applicationSecurityService;

        public OrderController(IOrderApplicationService orderApplicationService, IApplicationSecurityService applicationSecurityService)
        {
            _orderApplicationService = orderApplicationService;
            _applicationSecurityService = applicationSecurityService;
        }
        public async Task<IActionResult> Create()
        {
            var user = await _applicationSecurityService.GetCurrentUser(User);
            _orderApplicationService.CreateOrder(
                user.Id,
                Request.Cookies[Cookies.UserTrackingCode]
                );
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}