﻿using System.Diagnostics;
using a5pwt_ctvrtek.Models;
using Microsoft.AspNetCore.Mvc;

namespace a5pwt_ctvrtek.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}