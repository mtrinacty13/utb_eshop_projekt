﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Domain.Services.Files
{
    public interface IFileHandler
    {
        string saveImage(IFormFile file);
    }
}
