﻿using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Domain.Constants
{
    public class Roles
    {
        public const string User = "User";
		public const string Admin = "Admin";
        public const string Manager = "Manager";
		public const string UserSilver = "UserSilver";
		public const string UserGolden = "UserGolden";


		public const int UserRoleID = 1;
		public const int AdminRoleID = 2;
		public const int ManagerRoleID = 3;
		public const int UserSilverRoleID = 4;
		public const int UserGoldenRoleID = 5;

		public struct UserRoles
		{
			public int UserID;
			public int RoleID;
		}
	}
}
