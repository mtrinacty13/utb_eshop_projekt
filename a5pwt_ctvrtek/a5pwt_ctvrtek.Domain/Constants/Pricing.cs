﻿using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Domain.Constants
{
	public class Pricing
	{
		public const decimal Base = 1;
		public const decimal FirstClass = 0.99m;
		public const decimal SecondClass = 0.95m;
		public const decimal ThirdClass = 0.8m;

		public const int BaseAmount = 1;
		public const int FirstClassAmount = 10;
		public const int SecondClassAmount = 20;
		public const int ThirdClassAmount = 30;
	}
}
