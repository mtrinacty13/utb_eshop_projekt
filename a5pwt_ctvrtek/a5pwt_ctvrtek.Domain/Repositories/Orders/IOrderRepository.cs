﻿using System;
using System.Collections.Generic;
using System.Text;

namespace a5pwt_ctvrtek.Domain.Repositories.Orders
{
    public interface IOrderRepository
    {
        void CreateOrder(int userId, string userTrackingCode);
    }
}
