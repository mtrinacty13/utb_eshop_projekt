﻿using System;
using System.Collections.Generic;
using System.Text;
using a5pwt_ctvrtek.Domain.Entities.Adresses;


namespace a5pwt_ctvrtek.Domain.Entities.Orders
{
    public class Order:Entity
    {
        public int UserID { get; set; }
        public string OrderNumber { get; set; }
        public int? ShippingAdressID { get; set; }
        public int? BillingAdressID { get; set; }

        public IList<OrderItem> OrderItems { get; set; }
        public Address BillingAdress { get; set; }
        public Address ShippingAdress { get; set; }
    }
}
